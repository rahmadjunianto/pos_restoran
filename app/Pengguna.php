<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{

	protected $table = "T_PENGGUNA";
    protected $primaryKey = 'ID_PENGGUNA';
}
