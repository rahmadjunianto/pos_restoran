<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pengguna;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // session()->flash('flash_message', 'Sukses Login');
        // session()->flash('flash_type', 'success');
        // return view('home');
        $users=Pengguna::find(47);
        return $users->id_pengguna;
    }
}
